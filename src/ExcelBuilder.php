<?php

namespace Bitfoot\QueryBuilder;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;


class ExcelBuilder
{
    protected $mapping, $data;


    public function __construct(
        array $data
    ) {
        $this->data = $data;
        $this->mapping = array_keys((array) $this->data[0]);

        if (!empty($this->customFieldMapping)) {
            foreach ($this->customFieldMapping as $_originalFieldName => $_customFieldName) {
                $keyExists = array_keys($this->mapping, $_originalFieldName);

                if (isset($keyExists[0])) {
                    $this->mapping[$keyExists[0]] = $_customFieldName;
                }
            }
        }
    }

    /**
     * Function Explode
     * Validating filetype and filename.
     * Checking filename if filename is not provided then set it to null.
     * Returns filetype and filename.
     */
    public function explode($download){
        //  Exploding string download separated by coma.
        $download = explode(",", $download);

        // Validating filetype and filename.
        $validator = Validator::make($download, [
            "0" => ["required", "in:csv,xlsx"],
            "1" => ["sometimes", "string"]
        ], [
            "in" => "The filetype is invalid."
        ])->validate();

        // Returning validation error.
        if (!is_array($validator)) {
            return $validator;
        }

        return [$download[0], (isset($download[1]) && !empty($download[1])) ? $download[1] : null];
    }


    public function exporter($fileType, $fileName)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = ["font" => ["bold" => true]];
        $row = 2;

        // Create heading.
        $sheet->fromArray($this->mapping, NULL, "A1");

        // Insert each data row now in loop.
        foreach($this->data as $_eachDataRow) {
            $sheet->fromArray((array) $_eachDataRow, NULL, "A" . $row);
            $row++;
        }

        // Format style to the header row.
        $col = $spreadsheet->getActiveSheet()->getHighestDataColumn();
        $spreadsheet->getActiveSheet()->getStyle("A1:" . $col . "1")->applyFromArray($styleArray);

        // Autosize the columns.
        foreach(range("A", $col) as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        // Setting export to according to type
        if ($fileType == "csv") {
            $writer = new Csv($spreadsheet);
            $writer->setUseBOM(true);
            $writer->setDelimiter(",");
        } else {
            $writer = new Xlsx($spreadsheet);
        }

        // Checking if file name is null then set it to deafault filename.
        $fileName = $fileName == null ? "export" : $fileName;

        // If filename is not provided then add date to default filename.
        $fileName =  $fileName == "export" ? $fileName . "_" .  Carbon::now()->format("yy-m-d_h.i")  : $fileName;
        $randomFileName = mt_rand(100000, 999999) . "." . $fileType;
        $writer->save(storage_path($randomFileName));

        return response()->download(storage_path($randomFileName), $fileName . "." . $fileType)->deleteFileAfterSend(true);
    }


    // will create import method in future.
    // public function importer() {}
}

