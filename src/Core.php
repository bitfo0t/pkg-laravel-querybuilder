<?php

namespace Bitfoot\QueryBuilder;

use Illuminate\Support\Facades\Validator;
use App\Rules\Slug;
use Throwable;


trait Core
{
    protected $dtoMap, $errors, $dtoFileName;


    public function dtoMapping($type, $options = []) {
        try {
            $array = explode("\\", $this->className);
            $className = array_pop($array);
            $string = implode("\\", $array);
            $thisDto = $string . "\\Dto\\" . $className . "Dto" . (isset($options["dto"]) ? $options["dto"] : "1");

            $this->dtoMap = $thisDto::dtoMapping(isset($options["data"]) ? $options["data"] : []);
        } catch (Throwable $e) {}

        if ($type == "local") {
            $this->dtoMap["local"] = isset($this->dtoMap["local"]) ? $this->dtoMap["local"] : ["*"];
        } else {
            $this->dtoMap[$type] = isset($this->dtoMap[$type]) ? $this->dtoMap[$type] : [];
        }

        return $this->dtoMap[$type];
    }


    public function validate($data, $updateItemId = null, $foreignKeySometimes = false)
    {
        // When updating, we want to make sure only the incoming fields are picked for validation.
        if ($updateItemId) {
            // Update can have its open unique combination of validation rules. Get those and replace original key/val.
            foreach($this->updateValidationRules($updateItemId) as $_key => $_each) {
                $this->validationRules[$_key] = $_each;
            }
        }

        foreach ($this->validationRules as $key => $vals) {
            // This allows a child indirect store possible because the incoming request does not include key value.
            if ($foreignKeySometimes && ($key == $foreignKeySometimes)) {

                $this->validationRules[$key][array_search("required", $vals)] = "sometimes";
            }
            // Custom validation rules. Have to put this logic because new Slug doesn't work in validationRules in model.
            $lookForSlug = array_search('slug', $vals);

            if (!empty($lookForSlug)) {
                $this->validationRules[$key][$lookForSlug] = new Slug;
            }
        }

        // Make a new validator object.
        $v = Validator::make($data, $this->validationRules);

        // Check for failure.
        if ($v->fails()) {
//            $this->errors = $v->errors();
            $this->errors = $v->validate();

            return false;
        }

        // Validation pass.
        return true;
    }


    public function errors()
    {
        return $this->errors;
    }
}
