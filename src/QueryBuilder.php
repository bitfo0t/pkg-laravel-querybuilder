<?php

namespace Bitfoot\QueryBuilder;

use Illuminate\Support\Facades\DB;
use Bitfoot\QueryBuilder\ExcelBuilder;


trait QueryBuilder {

    /**
     * dtoConstruct
     * Takes a query (optional for now) and constructs a sql from it.
     * Useful for the running large dataset queries and paginating them.
     */
    public function dtoConstruct($options = [])
    {
        $fields = [];
        foreach($this->model->dtoMapping('local', $options) as $_k => $_each) {
            $fields[] = $_each;
        }

        $joins = [];
        foreach($this->model->dtoMapping('joins', $options) as $_k => $_each) {
            $tableVals = explode('|', $_each[1]);
            $tableName = (isset($tableVals[1]) ? $tableVals[1] . '.' : null) . $tableVals[0];

            if ($_each[0]) {
                foreach(explode(',', $_each[0]) as $_eachField) {
                    $fields[] = $tableName . '.' . trim($_eachField);
                }
            }

            // [1] = table name, [2] = joining field, [3] = self joining field, [4] where condition array
            $joins[] = [$tableName, $_each[2], $tableName . '.' . $_each[3], isset($_each[4]) ? $_each[4] : []];
        }

        $leftJoins = [];
        foreach($this->model->dtoMapping('leftJoins', $options) as $_k => $_each) {
            $tableVals = explode('|', $_each[1]);
            $tableName = (isset($tableVals[1]) ? $tableVals[1] . '.': null) . $tableVals[0];

            if ($_each[0]) {
                foreach(explode(',', $_each[0]) as $_eachField) {
                    $fields[] = $tableName . '.' . trim($_eachField);
                }
            }

            // [1] = table name, [2] = joining field, [3] = self joining field, [4] where condition array
            $leftJoins[] = [$tableName, $_each[2], $tableName . '.' . $_each[3], isset($_each[4]) ? $_each[4] : []];
        }

        $rawLeftJoins = [];
        foreach($this->model->dtoMapping('rawLeftJoins', $options) as $_k => $_each) {
            $tableVals = explode('|', $_each[1]);
            $tableName = (isset($tableVals[1]) ? $tableVals[1] . '.': null) . $tableVals[0];

            if ($_each[0]) {
                foreach(explode(',', $_each[0]) as $_eachField) {
                    $fields[] = trim($_eachField);
                }
            }

            // [1] = table name, [2] = joining field, [3] = self joining field, [4] where condition array
            $rawLeftJoins[] = [$tableName, $_each[2], $_each[3], isset($_each[4]) ? $_each[4] : []];
        }

        foreach($this->model->dtoMapping('rawJoins', $options) as $_k => $_each) {
            $fields[] = $_each[0];
            $joins[] = [$_each[1], $_each[2], $_each[1] . '.' . $_each[3], isset($_each[4]) ? $_each[4] : []];
        }

        $fields = DB::raw(implode(', ', $fields));

        $wheres = $this->model->dtoMapping('wheres', $options);

        $rawselect = implode(',', $this->model->dtoMapping('rawSelects', $options));

        $q = DB::table((method_exists($this->model, "getConnectionDb") ? $this->model->getConnectionDb() . '.' : null) . $this->model->getTable());

        if ($rawselect) {
            $q->select($fields, DB::raw($rawselect));
        } else {
            $q->select($fields);
        }

        foreach($joins as $_each) {
            if (!$_each[3]) {
                $q->join($_each[0], $_each[1], '=', $_each[2]);
            } else {
                $q->join($_each[0], function ($join) use ($_each) {
                    $join->on($_each[1], '=', $_each[2])
                         ->where($_each[3][0], $_each[3][1], $_each[3][2]);
                });
            }
        }

        foreach($leftJoins as $_each) {
            if (!$_each[3]) {
                $q->leftJoin($_each[0], $_each[1], '=', $_each[2]);
            } else {
                $q->leftJoin($_each[0], function ($join) use ($_each) {
                    $join->on($_each[1], '=', $_each[2])
                         ->where($_each[3][0], $_each[3][1], $_each[3][2]);
                });
            }
        }

        foreach($rawLeftJoins as $_each) {
            if (!$_each[3]) {
                $q->leftJoin($_each[0], $_each[1], '=', $_each[2]);
            } else {
                $q->leftJoin(DB::raw($_each[0]), function ($join) use ($_each) {
                    $join->on(DB::raw($_each[1]), '=', DB::raw($_each[2]))
                    ->whereRaw($_each[3]);
                });
            }
        }

        if (isset($options['sort']) && !empty($options['sort'])) {
            $sort = explode(',', $options['sort']);
            $q->orderBy($sort[0], isset($sort[1]) ? $sort[1] : 'asc');
        }

        if (isset($options['search'])) {
            $options['search'] = trim(urldecode($options['search']));

            if (!empty($options['search'])) {
                $q->where(function ($query) use ($options) {
                    foreach ($this->model->dtoMapping('searchFields', $options) as $_k => $_each) {
                        if (!$_k) {
                            $query->where($_each, 'like', '%' . $options['search'] . '%');
                        } else {
                            $query->orWhere($_each, 'like', '%' . $options['search'] . '%');
                        }
                    }
                });
            }
        }


        foreach($wheres as $_each) {
            $q->where($_each[0], $_each[1], $_each[2]);
        }

        /*
        At some point will have to think about when search and filter both are present. Laravel has
        Parameter Grouping that could be the solution. e.g.
        DB::table('users')
            ->where('name', '=', 'John')
            ->where(function ($query) {
                $query->where('votes', '>', 100)
                      ->orWhere('title', '=', 'Admin');
            })
            ->get();

        select * from users where name = 'John' and (votes > 100 or title = 'Admin')

        So in above example, outside could be search and inside bracket can be filter
        */
        //
        if (isset($options['filter'])) {
            $options['whereRaws'] = $this->dtoFilterConstruct($options['filter'], isset($options["dto"]) ? $options["dto"] : null);
            if (!empty($options['whereRaws'])) {
                $q->whereRaw($options['whereRaws']);
            }
        }

        // View constructed sql.
        if (isset($options['sql'])) {
            return $this->getSql($q);
        }

        if (isset($options["download"])) {
            $data = $q->get();

            $export = new ExcelBuilder($data->toArray());
            // This function explodes string, validate and return filetype and filename.
            $download = $export->explode($options["download"]);

            return $export->exporter($download[0], $download[1]);
        }

        if (isset($options['skip_pagination'])) {
            return $q->get();
        }

        return $q->paginate(
            isset($options['per_page']) && is_numeric($options['per_page']) ? $options['per_page'] : 20
        );
    }


    /**
     * Filter is considered a orWhere scenario. It's received as: filter: "status:whereOr:pending,approved".
     * This function takes the query string and constructs a raw or where query.
     *
     * @param $filterQuery
     * @return string
     */
    protected function dtoFilterConstruct($filterQuery, $data, $dto = null)
    {
        $whereRaws = '';

        $searchFields = $this->model->dtoMapping('searchFields', $data, $dto);

        $filterQuery = explode('|', $filterQuery);
        foreach($filterQuery as $_idx => $_eachQuery) {
            $thisWhereRaws = '';
            $thisFilter = explode(':', $_eachQuery);
            $valEx = explode(',', $thisFilter[2]);
            $thisWhere = explode('-', $thisFilter[1]);

            $thisWhereJoin = $thisFilter[1] == 'whereOr' || $thisFilter[1] == 'likeOr' ? ' or ' : ' and ';
            $thisExpression = $thisFilter[1] == 'likeOr' || $thisFilter[1] == 'likeOr' ? ' like ' : ' = ';

            if (isset($thisWhere[1])) {
                if (strtolower($thisWhere[1]) == "gt") {
                    $thisExpression = " > ";
                } else if (strtolower($thisWhere[1]) == "gteq") {
                    $thisExpression = " >= ";
                } else if (strtolower($thisWhere[1]) == "lt") {
                    $thisExpression = " < ";
                } else if (strtolower($thisWhere[1]) == "lteq") {
                    $thisExpression = " <= ";
                } else if (strtolower($thisWhere[1]) == "btw") {
                    $thisExpression = " BETWEEN ";
                    $valEx = [implode("' AND '", $valEx)];
                }
            }

            $thisWhereField = $searchFields[$thisFilter[0]];

            if (isset($thisWhere[2])) {
                if (strtolower($thisWhere[2]) == "date") {
                    $thisWhereField = "Date(" . $thisWhereField . ")";
                }
            }

            foreach ($valEx as $_eachVal) {
                // 'null' has a special case.
                if ($_eachVal == 'null') {
                    $thisWhereRaws .= ($thisWhereRaws ? $thisWhereJoin : '') . $searchFields[$thisFilter[0]] . ' is null';

                // 'not null' has a special case.
                } else if ($_eachVal == 'notnull') {
                    $thisWhereRaws .= ($thisWhereRaws ? $thisWhereJoin : '') . $searchFields[$thisFilter[0]] . ' is not null';

                // for all the rest.
                } else if (isset($_eachVal)) {
                    $thisWhereRaws .= ($thisWhereRaws ? $thisWhereJoin : '') . $searchFields[$thisFilter[0]] . $thisExpression . (($thisFilter[1] == 'like' || $thisFilter[1] == 'likeOr' ? "'%" : "'") .$_eachVal .( $thisFilter[1] == 'like' || $thisFilter[1] == 'likeOr' ? "%'" : "'"));
                }
            }

            $whereRaws .= (!$_idx ? '' : ' and ') . '(' . $thisWhereRaws . ')';
        }
        return $whereRaws;
    }


    /**
     * getSql
     * Used in dtoConstruct() for viewing the constructed sql for debugging.
     * @param $model
     * @return mixed
     */
    private function getSql($model)
    {
        $replace = function ($sql, $bindings)
        {
            $needle = '?';
            foreach ($bindings as $replace){
                $pos = strpos($sql, $needle);
                if ($pos !== false) {
                    if (gettype($replace) === "string") {
                        $replace = ' "'.addslashes($replace).'" ';
                    }
                    $sql = substr_replace($sql, $replace, $pos, strlen($needle));
                }
            }
            return $sql;
        };
        $sql = $replace($model->toSql(), $model->getBindings());

        return $sql;
    }
}
